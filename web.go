package web

import (
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/go-redis/redis"
)

// Global vars
var (
	RedisClient *redis.Client
	RgwClient   *s3.S3
	Endpoint    = "rgw.wka.se"
	BucketName  = "mediat"
)
