package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Podcast ...
type Podcast struct {
	Name  string `json:"name"`
	URL   string `json:"url"`
	ID    string `json:"id"`
	Image string `json:"image"`
}

// PodcastList ...
func PodcastList(c *gin.Context) {
	c.JSON(http.StatusOK, []Podcast{
		Podcast{"Historiepodden", "https://wka.se/histpod.xml", "historiepodden", ""},
		Podcast{"Hardcore History", "https://wka.se/hch.xml", "hch", ""},
	})
}
