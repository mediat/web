package web

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/mediat/ingest"
)

// UpdateAsset ...
func UpdateAsset(c *gin.Context) {
	var asset *ingest.Asset
	err := c.BindJSON(&asset)
	if err != nil {
		return
	}
	// Sanity check
	enc, err := json.Marshal(asset)
	if err != nil {
		c.Error(err)
		return
	}
	fmt.Println("got", string(enc))
	upload, ok := c.Params.Get("upload")
	if ok && upload == "true" {
		RedisClient.Publish(ingest.AssetUploadCommand, enc)
	}
	RedisClient.Publish(ingest.AssetAddCommand, enc)
	c.JSON(200, asset)
}
