FROM golang:1.9.2

WORKDIR /go/src/gitlab.com/mediat/web

ADD . .

RUN go-wrapper download
RUN go-wrapper install ./...

EXPOSE 8000

CMD ["go-wrapper", "run"]

