package main

import (
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/gin-gonic/gin"
	"gitlab.com/mediat/ingest"
	. "gitlab.com/mediat/web"
)

func main() {
	RedisClient = ingest.NewRedisClient()
	defer RedisClient.Close()
	sess := ingest.RgwSession()
	RgwClient = s3.New(sess)
	engine := gin.Default()
	engine.GET("/video", DbVideoList)
	engine.GET("/video/*id", VideoDetail)
	engine.GET("/podcast", PodcastList)
	engine.POST("/asset", UpdateAsset)
	engine.Run(":8000")
}
