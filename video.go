package web

import (
	"fmt"

	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"

	"path"
	"sort"
	"strings"

	"time"

	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/gin-gonic/gin"
	"gitlab.com/mediat/ingest"
)

type videoResponse struct {
	URL          string    `json:"url"`
	LastModified time.Time `json:"modified"`
	Name         string    `json:"name"`
}
type byName []videoResponse

func (a byName) Len() int           { return len(a) }
func (a byName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byName) Less(i, j int) bool { return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name) }

// VideoList ...
func VideoList(c *gin.Context) {
	resp, err := RgwClient.ListObjects(&s3.ListObjectsInput{
		Bucket: aws.String(BucketName),
		Prefix: aws.String("video"),
	})
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
	}
	videos := make([]videoResponse, 0, len(resp.Contents))
	for _, asset := range resp.Contents {
		videos = append(videos, videoResponse{
			URL:          fmt.Sprintf("https://%s/%s/%s", Endpoint, BucketName, *asset.Key),
			LastModified: *asset.LastModified,
			Name:         path.Base(*asset.Key),
		})
	}
	sort.Sort(byName(videos))
	c.JSON(http.StatusOK, videos)
}

// VideoDetail ...
func VideoDetail(c *gin.Context) {
	resp, err := RgwClient.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(BucketName),
		Key:    aws.String(c.Param("id")),
	})
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

var ddb *dynamodb.DynamoDB

func init() {
	sess := ingest.AwsSession()
	ddb = dynamodb.New(sess)
}

// DbVideoList ...
func DbVideoList(c *gin.Context) {
	cached, err := RedisClient.Get("videos").Result()
	if err == nil && cached != "" {
		c.Data(200, "application/json", []byte(cached))
		return
	}
	filter := expression.Name("mimeType").BeginsWith("video")
	expr, err := expression.NewBuilder().WithFilter(filter).Build()
	if err != nil {
		c.Error(err)
	}
	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		//ProjectionExpression:      expr.Projection(),
		TableName: aws.String("asset"),
	}
	result, err := ddb.Scan(params)

	if err != nil {
		c.Error(err)
	}
	var assets []ingest.Asset
	dynamodbattribute.UnmarshalListOfMaps(result.Items, &assets)
    // TODO ttl
	RedisClient.Set("videos", assets)
	c.JSON(200, assets)
}
